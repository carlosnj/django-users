from django.urls import path
from . import views

urlpatterns = [
	path('', views.index),
	path('addition/<int:num1>/<int:num2>', views.addition),
	path('substract/<int:num1>/<int:num2>', views.substract),
	path('multiply/<int:num1>/<int:num2>', views.multiply),
	path('split/<int:num1>/<int:num2>', views.split),			
]
