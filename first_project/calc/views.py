from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.


def index(request):
	return HttpResponse("Esta es una calculadora")

def addition(request, num1, num2):
	return HttpResponse(num1 + num2)

def substract(request, num1, num2):
        return HttpResponse(num1 - num2)

def multiply(request, num1, num2):
        return HttpResponse(num1 * num2)

def split(request, num1, num2):
        return HttpResponse(num1 / num2)
