from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
	return HttpResponse("<h1> Hello World! This is my first app in Django.</h1>")

def di_hola(request):
	return HttpResponse("Hello to everyone!")

def di_adios(request, nombre):
	return HttpResponse(f"<h1>Adios, {nombre}.</h1>")

def di_numeros(request, num1, num2):
	return HttpResponse(f"Numbers choosen were {str(num1)} and {str(num2)}")
	
