from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('login', views.loggedIn),  # Recursos específicos al principio.
    path('login/', views.loggedIn),
    path('logout', views.logoutView),
    path('logout/', views.logoutView),
    path('image/', views.image),
    path('<str:resource>', views.get_resources),  # Recursos genéricos al final.
    path('<str:resource>/', views.get_resources),
]
