from django.db import models


# Create your models here.

class Content(models.Model):
    clave = models.CharField(max_length=64)  # Resource (maximum 64 chars)
    valor = models.TextField()  # HTML Content (no chars limitations)

    def __str__(self):
        return f"{str(self.id)}: {self.clave}"

# relación de 1 a muchos, desde Content a Comment.
class Comment(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)  # "on_delete CASCADE" --> se si se elimina una
    # entrada en Content, se elimina el comment relacionado también
    title = models.CharField(max_length=128)
    comment = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return f"{str(self.id)}: {self.title} --> {self.content.clave}"

    def verify(self):
        return ("body" in self.comment)