@csrf_exempt
def get_resources(request, resource):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    elif request.method == "POST":
        valor = request.POST['valor']
    if request.method in ["PUT", "POST"]:
        try:
            c = m.Content.objects.get(clave=resource)
            c.valor = valor
        except m.Content.DoesNotExist:
            c = m.Content(clave=resource, valor=valor)
        c.save()

    content = get_object_or_404(m.Content, clave=resource)
    return HttpResponse(form.format(resource=resource, valor=content.valor))