from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from . import models as m
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect


form = """
        <!DOCTYPE html>
        <html lang="en">
          <body>
            <p></p>
            <form action="" method="POST">
              This is the resource: <h2>"{resource}"</h2> <br>
              Introduce a value to update it: <input name="valor" type="text" />
              <input type="submit" value="Submit" />
              <br><h3>Actual content: {value}</h3>
              <h4><a href="http://127.0.0.1:8000/login/">Login</a>,  
                <a href="http://127.0.0.1:8000/cms/logout/">Logout</a></h4>
            </form>
          </body>
        </html>
        """


def index(request):
    content_list = m.Content.objects.all()  # Obtener el contenido a tratar en el template.
    template = loader.get_template('cms/index.html')  # Cargar el template que vamos a usar
    contexto = {
     'content_list': content_list  # Crear el contexto que se enviará al template.
    }

    return HttpResponse(template.render(contexto, request))  # Renderizar el template y responder.


@csrf_exempt  # this means --> skip the security controls (we do it for now)
def get_resources(request, resource):
    if request.method == "PUT":
        value = request.body.decode('utf-8')  # We save the PUT body.
    elif request.method == "POST":
        value = request.POST['valor']  # We save the form content.

    # Manage the values in the database.
    if request.method in ["POST", "PUT"] and request.user.is_authenticated:
        try:
            c = m.Content.objects.get(clave=resource)  # Search for it, in case that exist.
            c.valor = value  # We set the new value to our object Content()
        except m.Content.DoesNotExist:
            c = m.Content(clave=resource, valor=value)  # If it does not exist, you create it.
        c.save()
    else:
        pass

    # request.method == 'GET':
    try:
        content = m.Content.objects.get(clave=resource)
        answer = HttpResponse(form.format(resource=resource, value=content.valor))
    except m.Content.DoesNotExist:
        if request.user.is_authenticated:
            content = ""
            answer = HttpResponse('The resource does not exist' + form.format(resource=resource, value=content))
        else:
            answer = HttpResponse('You are not logged in. Please <a href=/login/>login</a> or register.')

    return answer


def loggedIn(request):
    if request.user.is_authenticated:
        answer = f"The user '{request.user.username}' is authenticated. Redirection..." \
                 f"<meta http-equiv='refresh' content = '2;url=http://127.0.0.1:8000/cms/'>"
    else:
        answer = f"You are not logged in. Please <a href=/login/>login</a> or register."

    return HttpResponse(answer)


def logoutView(request):
    logout(request)

    return redirect("/cms/")


def image(request):
    template = loader.get_template('cms/template.html')
    context = {}

    return HttpResponse(template.render(context, request))